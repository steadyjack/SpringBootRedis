package com.boot.debug.redis.server.utils;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Snowflake;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

/**
 * 红包随机金额生成算法-二倍均值法
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2019/11/2 20:56
 **/
public class RedPacketUtil {

    /**
     * 二倍均值法的算法实现 - 算法里面的金额以 分 为单位
     * @param totalAmount
     * @param totalPeople
     * @return
     */
    public static List<Integer> divideRedPacket(final Integer totalAmount,final Integer totalPeople){
        List<Integer> list= Lists.newLinkedList();

        if (totalAmount>0 && totalPeople>0){
            Integer restAmount=totalAmount;
            Integer restPeople=totalPeople;

            Random random=new Random();
            int amount;
            for (int i=0;i<totalPeople-1;i++){
                //左闭右开 [1,剩余金额/剩余人数 的除数 的两倍 )

                amount=random.nextInt(restAmount/restPeople * 2 - 1)  + 1;
                list.add(amount);

                //剩余金额、剩余人数
                restAmount -= amount;
                restPeople--;
            }

            //最后一个剩余的金额
            list.add(restAmount);
        }
        return list;
    }

    /*public static void main(String[] args) {
        Integer sum=20;
        Integer total=5;

        List<Integer> list=divideRedPacket(sum,total);
        System.out.println("--"+list);

        Integer resSum=0;
        for (Integer a:list){
            resSum+=a;
        }
        System.out.println("拆分后最终的每个子项叠加起来--"+resSum);

        //TODO：map里面只有一个key、一个value的情况下
        Map<String,String> dataMap= Maps.newHashMap();
        dataMap.put("name","debug");
        Map.Entry<String,String> entry=dataMap.entrySet().iterator().next();
        System.out.println("key为："+entry.getKey() + " value为："+entry.getValue());

        String str="10:00:00";

        String[] arr=str.split(":");
        Calendar calendar=Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,Integer.valueOf(arr[0]));
        calendar.set(Calendar.MINUTE,Integer.valueOf(arr[1]));
        calendar.set(Calendar.SECOND,Integer.valueOf(arr[2]));

        Date time = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = df.format(time);
        System.out.println(format);

        System.out.println(new Snowflake(3,2).nextIdStr());
    }*/

}










































