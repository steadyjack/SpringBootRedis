# SpringBootRedis

#### 介绍
_**Spring Boot2.0 + 缓存中间件Redis（抢红包系统设计与实战） 实战之旅**_   
  
内容：除了介绍并实战Redis常见、常用的数据类型/数据结构之外，还实战了许许多多典型的应用场景，如商品信息有序存储、随机获取调查问卷试题、话费充值排行榜、系统数据字典Hash存储等等  

#### 文章列表  
1.Redis实战(1)-SpringBoot2.0整合Redis自定义注入模板操作Bean组件 ：http://www.fightjava.com/web/index/blog/article/60     
2.Redis实战(2)-数据结构之字符串String实战之存储对象 ：http://www.fightjava.com/web/index/blog/article/61     
3.Redis实战(3)-数据结构List实战一之商品信息的有序存储 ：http://www.fightjava.com/web/index/blog/article/62    
4.Redis实战(4)-数据结构List实战之队列特性实现消息多线程 广播通知 ：http://www.fightjava.com/web/index/blog/article/63    
5.Redis实战(5)-数据结构Set实战之过滤用户注册重复提交的信息 ：http://www.fightjava.com/web/index/blog/article/64      
6.Redis实战(6)-数据结构Set实战之获取随机乱序唯一的试卷题目 ： http://www.fightjava.com/web/index/blog/article/65    
7.Redis实战(7)-SortedSet之认识有序集合(命令行与代码实战) ： http://www.fightjava.com/web/index/blog/article/66   
8.Redis实战(8)-SortedSet典型应用场景实战之游戏充值排行榜 ： http://www.fightjava.com/web/index/blog/article/67    
9.Redis实战(9)-SortedSet实战之再谈游戏充值排行榜(如何处理历史与异常的充值记录) ：http://www.fightjava.com/web/index/blog/article/68     
10.Redis实战(10)-Hash实战之借助命令行和代码形式认识一下哈希 ：http://www.fightjava.com/web/index/blog/article/69     
11.Redis实战(11)-哈希Hash典型应用场景实战之系统数据字典实时触发缓存存储 ：http://www.fightjava.com/web/index/blog/article/70     
12.Redis实战(12)-基于Redis的Key失效和定时任务调度实现订单支付超时自动失效(延时队列)http://www.fightjava.com/web/index/blog/article/71     
13.Redis实战(13) - 手把手搭建Redis集群环境 (3主3从) ：http://www.fightjava.com/web/index/blog/article/72    
14....

#### 备注

观看完整的视频教程，可以前往debug搭建的技术社区进行观看学习：http://www.fightjava.com/web/index/course/detail/12  

![alt 整体内容大纲](http://www.fightjava.com/files/fightCoding/course/20191105/409802398363591292.png)  
![alt 核心技术列表](http://www.fightjava.com/files/fightCoding/course/20191105/409801751849302153.png)    
![alt 列表List的应用场景](http://www.fightjava.com/files/fightCoding/course/20191105/409803516816696531.png)    
![alt 集合Set的应用场景](http://www.fightjava.com/files/fightCoding/course/20191105/409803611742936696.png)    


**debug的技术公众号**：  
![alt 技术公众号](https://images.gitbook.cn/daec11f0-b1b2-11e9-8b37-8b327485b95f)  





