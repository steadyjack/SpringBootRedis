package com.boot.debug.redis.server.service;

import cn.hutool.core.lang.Snowflake;
import com.boot.debug.redis.model.entity.UserOrder;
import com.boot.debug.redis.model.mapper.UserOrderMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**用户下单service
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/1/3 13:30
 **/
@EnableScheduling
@Service
public class UserOrderService {

    private static final Logger log= LoggerFactory.getLogger(UserOrderService.class);

    //雪花算法生成订单编号
    private static final Snowflake SNOWFLAKE=new Snowflake(3,2);

    //存储至缓存的用户订单编号的前缀
    private static final String RedisUserOrderPrefix="SpringBootRedis:UserOrder:";

    //用户订单失效的时间配置 - 30min
    private static final Long UserOrderTimeOut=30L;


    @Autowired
    private UserOrderMapper userOrderMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 下单服务
     * @param entity
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public String putOrder(UserOrder entity) throws Exception{
        //用户下单-入库
        String orderNo=SNOWFLAKE.nextIdStr();
        entity.setOrderNo(orderNo);
        entity.setOrderTime(DateTime.now().toDate());
        int res=userOrderMapper.insertSelective(entity);

        if (res>0){
            //TODO:入库成功后-设定TTL 插入缓存 - TTL一到，该订单对应的Key将自动从缓存中被移除(间接意味着：延迟着做某些时间)
            stringRedisTemplate.opsForValue().set(RedisUserOrderPrefix+orderNo,entity.getId().toString(),UserOrderTimeOut, TimeUnit.MINUTES);
        }
        return orderNo;
    }


    //TODO：定时任务调度-拉取出 有效 + 未支付 的订单列表，前往缓存查询订单是否已失效
    @Scheduled(cron = "0 0/5 * * * ?")
    @Async("threadPoolTaskExecutor")
    public void schedulerCheckOrder(){
        try {
            List<UserOrder> list=userOrderMapper.selectUnPayOrders();
            if (list!=null && !list.isEmpty()){

                /*for (UserOrder entity:list){
                    final String orderNo=entity.getOrderNo();
                    String key=RedisUserOrderPrefix+orderNo;
                    log.info("{}",key);
                }*/

                list.forEach(entity -> {
                    final String orderNo=entity.getOrderNo();
                    String key=RedisUserOrderPrefix+orderNo;
                    if (!stringRedisTemplate.hasKey(key)){
                        //TODO:表示缓存中该Key已经失效了，即“该订单已经是超过30min未支付了，得需要前往数据库将其失效掉”
                        userOrderMapper.unActiveOrder(entity.getId());
                        log.info("缓存中该订单编号已经是超过指定的时间未支付了，得需要前往数据库将其失效掉！orderNo={}",orderNo);
                    }
                });
            }
        }catch (Exception e){
            log.error("定时任务调度-拉取出 有效 + 未支付 的订单列表，前往缓存查询订单是否已失效-发生异常：",e.fillInStackTrace());
        }
    }


}
















